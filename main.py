from typing import List
from datetime import datetime

import databases
import sqlalchemy
from sqlalchemy.orm import relationship, Session, sessionmaker
from fastapi import Depends, FastAPI, HTTPException
from pydantic import BaseModel
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm


DATABASE_URL = "postgresql://laves:laves@127.0.0.1:5432/postgres"

database = databases.Database(DATABASE_URL)

metadata = sqlalchemy.MetaData()


activities = sqlalchemy.Table(
    "activities",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String),
)


conditions = sqlalchemy.Table(
    "conditions",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String),
)


plants = sqlalchemy.Table(
    "plants",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String),
)


users = sqlalchemy.Table(
    "users",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("username", sqlalchemy.String),
    sqlalchemy.Column("password", sqlalchemy.String),
)


engine = sqlalchemy.create_engine(
    DATABASE_URL,
)
metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class User(BaseModel):
    id: int
    username: str
    password: str

    class Config:
        orm_mode = True


class Activity(BaseModel):
    id: int
    name: str


class Plant(BaseModel):
    id: int
    name: str
    sowing_date: datetime


class Condition(BaseModel):
    id: int
    name: str


app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/")
async def read_root(token = Depends(oauth2_scheme)):
    return {"Hello": "World"}


@app.post("/token")
async def token(form_data: OAuth2PasswordRequestForm = Depends()):
    print('Token endpoint')
    if not form_data:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    all_users = session.query(users).all()
    for user in all_users:
        if not form_data.password == user.password:
            raise HTTPException(status_code=400, detail="Incorrect username or password")

    return {"access_token": form_data.username, "token_type": "bearer"}


@app.get("/register")
async def register_user(form_data: OAuth2PasswordRequestForm = Depends()):
    print('Register user endpoint')
    query = users.insert().values(username=form_data.username, password=form_data.password)
    db_user = await database.execute(query)
    return db_user


# @app.get("/notes/", response_model=List[User])
# async def get_users():
#     query = users.select()
#     return await database.fetch_all(query)
